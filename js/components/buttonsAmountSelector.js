(function () {
	'use strict';

	var DEFAULT_AMOUNTS = [25, 50, 100, 200];
	
	PaymentProto.ButtonsAmountSelectorComponent = Ember.Component.extend({
		layoutName:'components/buttons-amount-selector',
		
		predefinedAmounts: DEFAULT_AMOUNTS,
		
		actions: {
			setAmount: function(amount) {
				this.set('amount', amount);
			}
		}
	});
})();
