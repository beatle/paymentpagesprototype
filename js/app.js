window.PaymentProto = Ember.Application.create();
Ember.Application.initializer({
   name: "setup",
   initialize: function(container, application) { 
		// application.register('component:amount-selector', PaymentProto.SliderAmountSelectorComponent); 
		application.register('component:amount-selector', PaymentProto.ButtonsAmountSelectorComponent); 
		application.inject('route:deposit.ecmc', 'template:components/amount-selector', "component:slider-amount-selector"  ); 
	} 
});

PaymentProto.ApplicationAdapter = DS.LSAdapter.extend({
	namespace: 'payment-proto'
});
