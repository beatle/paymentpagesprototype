(function (PaymentProto) {
	'use strict';

	PaymentProto.Router.map(function () {
		this.resource('deposit', function () {
            this.route('visa');
			this.route('ecmc');
			this.route('maestro');
		});
        this.resource('visa');
		this.resource('withdraw');
	});

    PaymentProto.DepositRoute = Ember.Route.extend({
        actions: {
            didTransition: function() {
                var lastPaymentMethodRoute = this.get('controller').get('lastPaymentMethodRoute');
                this.transitionTo(lastPaymentMethodRoute || 'deposit.visa');
                return true;
            }
        }
    });
 
    PaymentProto.DepositMethodRoute = Ember.Route.extend({
        templateName: 'deposit/method',

        activate: function() {
            var controller = this.controllerFor('deposit');
            controller.set('lastPaymentMethodRoute', this.get('routeName'));
        },

        model: function() {
            return this.store.createRecord('depositCard');
        },

        setupController: function(controller, model) {
            controller.set('model', model);
            controller.set('title', 'Card Deposit');
        }
    });

    PaymentProto.DepositVisaRoute = PaymentProto.DepositMethodRoute.extend({ 
        setupController: function(controller, model) {
            this._super(controller, model);
            controller.set('title', 'Visa Deposit');
        }
    });

    PaymentProto.DepositEcmcRoute = PaymentProto.DepositMethodRoute.extend({ });
    PaymentProto.DepositMaestroRoute = PaymentProto.DepositMethodRoute.extend({ });
})(PaymentProto);

