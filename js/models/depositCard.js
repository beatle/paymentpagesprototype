(function () {
	'use strict';

	PaymentProto.DepositCard = DS.Model.extend({
		cardNumber: DS.attr('string'),
		expirationDate: DS.attr('date', {
            defaultValue: function() { return new Date(); }
        }),
        amount: DS.attr('number', {defaultValue: 0}),
        currencyCode: DS.attr('string', {defaultValue: 'EUR'})
	});
})();
