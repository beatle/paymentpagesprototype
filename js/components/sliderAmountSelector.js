(function () {
	'use strict';

	PaymentProto.SliderAmountSelectorComponent = Ember.Component.extend({
		layoutName:'components/slider-amount-selector',
		
		actions: {
			setAmount: function(amount) {
				this.set('amount', amount);
			}
		}
	});
})();

